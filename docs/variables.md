## Role Variables

* `debian_ami_packages`: `[]` - additional packages to install



* `debian_ami_with_ansible`: `false` - if true will install ansible for use with aws ssm



* `debian_ami_boto3_version`: `1.14.55` - Version of boto3 to install from pypi



* `debian_ami_ansible_version`: `2.9.13` - Version of ansible to install from pypi


