# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.2.7](https://gitlab.com/guardianproject-ops/ansible-debian-ami/compare/0.2.6...0.2.7) (2022-02-01)


### Features

* Add systemd-journald configuration values to control disk usage ([6fc4425](https://gitlab.com/guardianproject-ops/ansible-debian-ami/commit/6fc4425d32c0035d3665a733ff43fdc189f39f4c))

### [0.2.6](https://gitlab.com/guardianproject-ops/ansible-debian-ami/compare/0.2.5...0.2.6) (2021-12-13)


### Features

* tag aws-env-load script to allow for targeted updates ([cad9f1a](https://gitlab.com/guardianproject-ops/ansible-debian-ami/commit/cad9f1a3867d1489681eadf56ee72a1cb4031033))

### [0.2.5](https://gitlab.com/guardianproject-ops/ansible-debian-ami/compare/0.2.4...0.2.5) (2021-12-13)


### Bug Fixes

* Update aws-env-load to use IMDsv2 token endpoint ([466441c](https://gitlab.com/guardianproject-ops/ansible-debian-ami/commit/466441c4acb6bd8253c450445d2468b15280d75a)), closes [#1](https://gitlab.com/guardianproject-ops/ansible-debian-ami/issues/1)

### [0.2.5](https://gitlab.com/guardianproject-ops/ansible-debian-ami/compare/0.2.4...0.2.5) (2021-12-13)


### Bug Fixes

* Update aws-env-load to use IMDsv2 token endpoint ([466441c](https://gitlab.com/guardianproject-ops/ansible-debian-ami/commit/466441c4acb6bd8253c450445d2468b15280d75a)), closes [#1](https://gitlab.com/guardianproject-ops/ansible-debian-ami/issues/1)

### [0.2.4](https://gitlab.com/guardianproject-ops/ansible-debian-ami/compare/0.2.3...0.2.4) (2021-10-11)


### Bug Fixes

* Ensure we are using python3 ([ef803e1](https://gitlab.com/guardianproject-ops/ansible-debian-ami/commit/ef803e13e11b632f206752dc93a1624130ca9bd5))

### [0.2.3](https://gitlab.com/guardianproject-ops/ansible-debian-ami/compare/0.2.2...0.2.3) (2021-10-07)


### Features

* automatically bundle the aws ca certs in the system store ([293328d](https://gitlab.com/guardianproject-ops/ansible-debian-ami/commit/293328d368dccd40aa7de9ef70ad9e7d57a07250))

### [0.2.2](https://gitlab.com/guardianproject-ops/ansible-debian-ami/compare/0.2.1...0.2.2) (2021-03-02)


### Features

* update motd with instance data and show to users on login ([f90b7a6](https://gitlab.com/guardianproject-ops/ansible-debian-ami/commit/f90b7a65dcb6735aabdabd4178e1c3e42d8e860d))
* upgrade default ansible and boto3 versions ([ab544ab](https://gitlab.com/guardianproject-ops/ansible-debian-ami/commit/ab544ab6ba8b2a82c608d5dfde8a640ca961ac0b))

## [0.2.1] - 2020-04-16

### Added

- Add instance profile arn and id to the metadata output

### Changed

n/a

### Removed

n/a

## [0.2.0] - 2020-04-15

### Added

- Output aws env info to /etc/ansible/facts.d as json for use with ansible

### Changed

n/a

### Removed

n/a

## [0.1.0] - 2020-04-10

### Added

- This CHANGELOG

### Changed

n/a

### Removed

n/a

[Unreleased]: https://gitlab.com/guardianproject-ops/ansible-debian-ami/compare/0.1.0...main
[0.1.0]: https://gitlab.com/guardianproject-ops/ansible-debian-ami/-/tags/0.1.0
[0.2.0]: https://gitlab.com/guardianproject-ops/ansible-debian-ami/compare/0.1.0...0.2.0
[0.2.1]: https://gitlab.com/guardianproject-ops/ansible-debian-ami/compare/0.2.0...0.2.1
