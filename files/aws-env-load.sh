#!/usr/bin/env bash
#
# This script is designed to be run at boot on EC2 instances
# with the 'ec2:DescribeTags' permission.
#
# It creates the file /etc/aws-environment which can be used in systemd units
# via EnvironmentFile=/etc/aws-environment
#
# Furthermore it creates /etc/profile.d/50-aws-environment.sh
# which exports the variables into a user's env
# 
# The list of env variables that are set are:
# 
#   * AWS_REGION
#   * AWS_INSTANCE_ID
#   * AWS_AZ
#
# Furthermore, all tags on the instance are exported with the TAG_ prefix
# and any non [a-zA-Z0-9_] characters are replaced with underscores.
#
# Author:
#   Abel Luck <abel@guardianproject.info>
# License:
#   AGPL-3.0-or-later
#   https://spdx.org/licenses/AGPL-3.0-or-later.html

set -eo pipefail

if ! command -v aws  &>/dev/null; then
  echo "ERROR: 'AWS CLI' Not installed"
  exit 1
fi

if ! command -v curl &>/dev/null; then
  echo "ERROR: 'curl' not installed"
  exit 1
fi

TOKEN=$(curl -s -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 300")
INSTANCE_ID=$(curl -s -H "X-aws-ec2-metadata-token: $TOKEN" http://169.254.169.254/latest/meta-data/instance-id)
if [[ -z "$INSTANCE_ID" ]]; then
  echo "ERROR: instance id could not be read from metadata endpoint (using IMDSv2 http tokens)"
  exit 1
fi
EC2_AVAIL_ZONE=$(curl -s -H "X-aws-ec2-metadata-token: $TOKEN" http://169.254.169.254/latest/meta-data/placement/availability-zone)
EC2_REGION="$(echo "$EC2_AVAIL_ZONE" | sed 's/[a-z]$//')"
EC2_INSTANCE_PROFILE_ARN=$(curl -s -H "X-aws-ec2-metadata-token: $TOKEN" http://169.254.169.254/latest/meta-data/iam/info | jq -r '.InstanceProfileArn')
EC2_INSTANCE_PROFILE_ID=$(curl -s -H "X-aws-ec2-metadata-token: $TOKEN" http://169.254.169.254/latest/meta-data/iam/info | jq -r '.InstanceProfileId')


set +e
TAG_OUTPUT=$(aws ec2 describe-tags --region $EC2_REGION --filters "Name=resource-id,Values=$INSTANCE_ID" 2> /dev/null )
awsfail="$?"
set -e
if [ "$awsfail" == 0 ]; then
  TAGS=$(echo "$TAG_OUTPUT" | \
          python3 -c "import re, sys,json; map(lambda t: sys.stdout.write('TAG_%s=%s\n' % (re.sub('[^a-zA-Z0-9_]','_', t['Key']), t['Value'])), json.load(sys.stdin)['Tags'])")
  TAG_KEYS=$(echo "$TAG_OUTPUT" | \
          python3 -c "import re, sys,json; map(lambda t: sys.stdout.write('TAG_%s <redacted>\n' % (re.sub('[^a-zA-Z0-9_]','_', t['Key']))), json.load(sys.stdin)['Tags'])")

  TAGS_YAML=$(echo "$TAG_OUTPUT" | \
    python3 -c "import re, sys,json; map(lambda t: sys.stdout.write('aws_tag_%s: %s\n' % (re.sub('[^a-zA-Z0-9_]','_', t['Key'].lower()), t['Value'])), json.load(sys.stdin)['Tags'])")

  TAGS_JSON=$(echo "$TAG_OUTPUT" | \
    python3 -c "import re, sys,json; print(json.dumps(dict(map(lambda t: ('%s' % (re.sub('[^a-zA-Z0-9_]','_', t['Key'].lower())), t['Value']), json.load(sys.stdin)['Tags']))))")
else
  TAGS=""
  TAG_KEYS=""
  TAG_YAML=""
  TAG_JSON=""
fi

cat << EOF > /etc/aws-environment
AWS_REGION=${EC2_REGION}
AWS_AZ=${EC2_AVAIL_ZONE}
AWS_INSTANCE_ID=${INSTANCE_ID}
AWS_INSTANCE_PROFILE_ARN=${EC2_INSTANCE_PROFILE_ARN}
AWS_INSTANCE_PROFILE_ID=${EC2_INSTANCE_PROFILE_ID}
${TAGS}
EOF

cat << EOF > /etc/aws-environment.yml
---
aws_region: ${EC2_REGION}
aws_az: ${EC2_AVAIL_ZONE}
aws_instance_id: ${INSTANCE_ID}
aws_instance_profile_arn: ${EC2_INSTANCE_PROFILE_ARN}
aws_instance_profile_id: ${EC2_INSTANCE_PROFILE_ID}
${TAGS_YAML}
EOF

cat << EOF > /etc/profile.d/50-aws-environment.sh
set -o allexport
source /etc/aws-environment
EOF

mkdir -p /etc/ansible/facts.d

cat << EOF > /etc/ansible/facts.d/aws_instance.fact
{
"region": "${EC2_REGION}",
"az": "${EC2_AVAIL_ZONE}",
"instance_id": "${INSTANCE_ID}",
"instance_profile_arn": "${EC2_INSTANCE_PROFILE_ARN}",
"instance_profile_id": "${EC2_INSTANCE_PROFILE_ID}"
}
EOF

cat << EOF > /etc/ansible/facts.d/aws_tags.fact
${TAGS_JSON}
EOF

sudo chmod +x /etc/profile.d/50-aws-environment.sh

divider================================================
divider=$divider$divider
format="%-30s    %-15s\n"
width=60
printf "%$width.${width}s\n" "$divider"
printf  "aws-env-load\n"
printf "%$width.${width}s\n" "$divider"
printf "$format" \
  AWS_INSTANCE $INSTANCE_ID \
  AWS_AZ          $EC2_AVAIL_ZONE\
  AWS_REGION      $EC2_REGION \
  $TAG_KEYS
printf "%$width.${width}s\n" "$divider"
echo "Wrote:"
echo "   /etc/aws-environment"
echo "   /etc/aws-environment.yml"
echo "   /etc/profile.d/50-aws-environment.sh"
echo "   /etc/ansible/facts.d/aws_instance.fact"
echo "   /etc/ansible/facts.d/aws_tags.fact"
if [ "$awsfail" != 0 ]; then
echo ""
echo "WARNING:"
echo "  EC2 Instance tags not parsed. Does the instance"
echo "  have the ec2:DescribeTags permission?"
fi
printf "%$width.${width}s\n" "$divider"
